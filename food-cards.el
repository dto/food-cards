;;; food-cards.el --- customizable SVG cards for meal planning

;; Copyright (C) 2018, 2019, 2022 by David O'Toole

;; Author: David O'Toole <deeteeoh1138@gmail.com>
;; Keywords: games
;; License: MIT (see below)

;; Permission is hereby granted, free of charge, to any person obtaining a copy
;; of this software and associated documentation files (the "Software"), to deal
;; in the Software without restriction, including without limitation the rights
;; to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
;; copies of the Software, and to permit persons to whom the Software is
;; furnished to do so, subject to the following conditions:

;; The above copyright notice and this permission notice shall be included in all
;; copies or substantial portions of the Software.

;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
;; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
;; AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
;; OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
;; SOFTWARE.

;;; Commentary:

;; See the included file "README.md" for full commentary.

;; The main command is "M-x fc-make-all-cards". You should customize
;; the variables first. See the bottom of the file for an example
;; customization.

;;; Code:

(require 'cl)
(require 'svg)

(defmacro defvar* (symbol initvalue &optional docstring)
  `(progn (defvar ,symbol nil docstring)
	  (setf ,symbol ,initvalue)))

;;; Customization variables

(defvar* fc-version-string "v2.6i")
(defvar* fc-card-width 2.2) ;; in inches
(defvar* fc-card-height 3.43) ;; in inches
(defvar* fc-card-header-height 0.8) ;; in inches
(defvar* fc-x-offset 0.32) ;; in inches
(defvar* fc-y-offset 0.558) ;; in inches

(defvar* fc-background-color "white")

(defvar* fc-text-font-color "#000000")
(defvar* fc-text-font-family "sans-serif")
(defvar* fc-text-font-size "11.2")
(defvar* fc-text-font-weight "normal")

(defvar* fc-title-font-color "#ffffff")
(defvar* fc-title-font-family "sans-serif")
(defvar* fc-title-font-size "16")
(defvar* fc-title-font-weight "normal")

(defvar* fc-inkscape-program "inkscape");; change this if you need to customize the path

(defvar* fc-category-colors '("#ee0000" "#8b4513" "#00bfff" "#228b22" "#db7093" "#ffd700" "#bbbbbb"))
(defvar* fc-categories '(:protein :starch :dairy :vegetable :fruit :fat :meal))

;;; Meal template cards

;; (defvar* fc-meal-cards '(

;;; Activity cards

(defvar* fc-meal-cards '(
"Beans + rice"
"Paneer + rice (or naan)"
"Potatoes + rice (or naan)"
"Bran flakes + fruit"
"Oatmeal + nuts"
"Quinoa + vegetables"
"Soy protein + noodles"
"Grilled cheese + soup"
"Scrambled eggs + toast"
"Banana dosas"
"Fruit + nuts"
"Fruit + cheese"
"Protein shake + fruit"
"Soup and salad"
"Veggie burger"
))


(defvar* fc-protein-cards '(
                            "soya protein"
                            "chickpeas"
                            "eggs"
                            "quinoa"
                            "almonds"
                            "veggie burger"
                            "peanut butter"
                            ))

(defvar* fc-starch-cards '("white rice"
                           "jasmine rice"
                           "brown rice"
                           "basmati rice"
                           "yellow rice"
                           "whole wheat naan"
                           "wheat bread"
                           "rice noodles"
                           "wheat noodles"
                           "wheat dosas"))

(defvar* fc-dairy-cards '("whole milk"
                          "skim milk"
                          "nonfat dry milk"
                          "cheese"))

(defvar* fc-vegetable-cards '("tomato" "celery" "kale" "romaine lettuce" "iceberg lettuce" "beets" "broccoli" "cauliflower" "corn" "green beans"))

(defvar* fc-fruit-cards '("mango" "blueberries" "blackberries" "apricots" "peaches" "apples" "asian pears" "bosc pears" "donut peaches"))

(defvar* fc-fat-cards '("olive oil" "sesame oil"))

(defvar* fc-back-cards
    (list (concat "Vegetarian Planner" fc-version-string)))
			
;;; SVG Card generation

(defun fc-category-color (category)
  (let ((n (position category fc-categories)))
    (if (numberp n)
	;; use last color if too many categories
	(nth (min n (1- (length fc-category-colors)))
	     fc-category-colors)
      (car (last fc-category-colors)))))

(defun fc-inches (number)
  (format "%Sin" number))

(defun fc-pixels (number)
  (format "%Sin" number))

(defun fc-blank-card ()
  (svg-create (fc-inches fc-card-width)
	      (fc-inches fc-card-height)))

(defvar* fc-card-number 0)
(defvar* fc-current-card nil)
(defvar* fc-x 0.0)
(defvar* fc-y 0.0)
(defvar* fc-tx 0.0)
(defvar* fc-ty 0.0)

(defun fc-reset-card ()
  (setf fc-current-card (fc-blank-card))
  (setf fc-x fc-x-offset)
  (setf fc-y fc-y-offset)
  (setf fc-tx 0.0)
  (setf fc-ty 0.0))

(defun* fc-draw-card (category title text &optional (x 0) (y 0))
  ;; draw background
  (svg-rectangle fc-current-card
		 (fc-inches x)
		 (fc-inches y)
		 (fc-inches fc-card-width)
		 (fc-inches fc-card-height)
		 :fill fc-background-color)
  ;; draw header color
  (svg-rectangle fc-current-card
		 (fc-inches x)
		 (fc-inches y)
		 (fc-inches fc-card-width)
		 (fc-inches fc-card-header-height)
		 :fill (fc-category-color category))
  ;; draw title text
  (svg-text fc-current-card
	    title
	    :font-size fc-title-font-size
	    :font-weight fc-title-font-weight
	    :fill fc-title-font-color
	    :font-family fc-title-font-family
	    :x (fc-inches (+ x fc-x-offset))
	    :y (fc-inches (+ y fc-y-offset)))
  ;; draw lines
  (fc-draw-paragraph (split-string text "\n") (+ x fc-x-offset) y)
  ;; draw card number
  (unless (zerop fc-card-number)
    (svg-text fc-current-card (format "%S" fc-card-number)
	      :font-size "9"
	      :font-weight fc-text-font-weight
	      :fill fc-text-font-color
	      :font-family fc-text-font-family
	      :x (fc-inches 1.7)
	      :y (fc-inches 3.1))))

(defun fc-draw-paragraph (lines x y)
  (when lines
    (let ((baseline (+ y fc-card-header-height 0.4)))
      (dolist (line lines)
	(svg-text fc-current-card
		  line
		  :font-size fc-text-font-size
		  :fill fc-text-font-color
		  :font-weight fc-text-font-weight
		  :font-family fc-text-font-family
		  :x (fc-inches (+ x 0.0))
		  :y (fc-inches baseline))
	(incf baseline 0.15)))))

;;; Generating all the cards

(defun fc-write-card ()
  (with-temp-buffer
    (svg-print fc-current-card)
    (write-file (format "card-%S.svg" fc-card-number))))

(defun fc-convert-card ()
  (ignore-errors
    (call-process fc-inkscape-program
		  nil '(:file "fc-errors.txt") nil
		  (format "--export-png=card-%S.png" fc-card-number)
		  "--export-dpi=600"
		  (format "card-%S.svg" fc-card-number))))

(defun fc-make-card (category title text)
  (fc-reset-card)
  (message "Generating card %S..." fc-card-number)
  (fc-draw-card category title text)
  (message "Generating card %S... Done." fc-card-number)
  (fc-write-card)
  (fc-convert-card)
  (message "Finished card %S." fc-card-number)
  (incf fc-card-number))

(defun fc-make-all-cards ()
  (interactive)
  (setf fc-card-number 0)
  (setf fc-background-color "#888888")
  (setf fc-text-font-color "#ffffff")
  ;; (fc-make-card :back " " (first fc-back-cards))
  (setf fc-background-color "white")
  (setf fc-text-font-color "#000000")
  (dolist (meal fc-meal-cards)
    (fc-make-card :meal "meal" meal))
  (dolist (protein fc-protein-cards)
    (fc-make-card :protein "protein" protein))
  (dolist (starch fc-starch-cards)
    (fc-make-card :starch "starch" starch))
  (dolist (dairy fc-dairy-cards)
    (fc-make-card :dairy "dairy" dairy))
  (dolist (vegetable fc-vegetable-cards)
    (fc-make-card :vegetable "vegetable" vegetable))
  (dolist (fruit fc-fruit-cards)
    (fc-make-card :fruit "fruit" fruit))
  (dolist (fat fc-fat-cards)
    (fc-make-card :fat "fat" fat))
  )
;;; Example customization

(defun fc-test ()
  (interactive)
  (setf fc-title-font-family "Roboto")
  (setf fc-text-font-family "Roboto")
  (fc-make-all-cards))

;;; Postamble

(provide 'food-cards)

;; Local Variables:
;; lexical-binding: t
;; End:

;;; food-cards.el ends here
